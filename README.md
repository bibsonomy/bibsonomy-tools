bibsonomy-tools
===============

Tools for BibSonomy:

- [DocumentDownloader](https://bitbucket.org/bibsonomy/bibsonomy-tools/src/default/src/main/java/org/bibsonomy/tools/DocumentDownloader.java): download all documents from your user account (a restriction by tag(s) is possible) 
- [UrlListScraper](https://bitbucket.org/bibsonomy/bibsonomy-tools/src/default/src/main/java/org/bibsonomy/tools/UrlListScraper.java): Reads a list of URLs and tries to gather publication metadata from them using BibSonomy's screen scrapers. 

compiling
---------

You need Mercurial, Java and Maven to download and compile the
code. Then you can do:

```shell
hg clone ssh://hg@bitbucket.org/bibsonomy/bibsonomy-tools
cd bibsonomy-tools
mvn install
```
You can find the compiled JAR file in the `target` folder. 

running
-------

You can run, e.g., the `UrlListScraper` by

```shell
java -cp target/bibsonomy-tools-0.0.1-SNAPSHOT-jar-with-dependencies.jar org.bibsonomy.tools.UrlListScraper
```

It is then waiting for input from standard in. Entering a URL like
http://www.springerlink.com/index/10.1007/978-3-642-25694-3_3 causes
it to get the web site, extract the metadata and print the resulting
BibTeX.



licensing 
--------- 

Please see the file [LICENSE.txt](https://bitbucket.org/bibsonomy/bibsonomy-tools/src/default/LICENSE.txt).

homepage
--------

[https://bitbucket.org/bibsonomy/bibsonomy-tools](https://bitbucket.org/bibsonomy/bibsonomy-tools)
