/**
 *
 *  BibSonomy Tools - Useful tools for use with BibSonomy
 *
 *  Copyright (C) 2013 - 2013 Knowledge & Data Engineering Group,
 *                            University of Kassel, Germany
 *                            http://www.kde.cs.uni-kassel.de/
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.bibsonomy.tools;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.bibsonomy.common.enums.Filter;
import org.bibsonomy.common.enums.FilterEntity;
import org.bibsonomy.common.enums.GroupingEntity;
import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.enums.Order;
import org.bibsonomy.model.logic.LogicInterface;
import org.bibsonomy.rest.client.RestLogicFactory;
import org.bibsonomy.util.Sets;

/**
 * Given a userName, apiKey and (optionally) tags, downloads all documents of the
 * corresponding posts to the current directory.
 * 
 * @author:  rja
 * 
 */
public class PostClusterer {

	private static final int MAX_POSTS = 1000;

	private final LogicInterface logic;

	public PostClusterer(final String userName, final String apiKey) {
		this.logic = new RestLogicFactory().getLogicAccess(userName, apiKey);
	}

	public static void main(final String[] args) throws IOException {
		if (args.length < 2) {
			System.err.println("usage: java " + PostClusterer.class.getName() + " userName apiKey k [tags]");
			System.err.println("clusters the posts with the given tag(s) into k clusters");
			System.exit(1);
		}
		int argctr = 0;
		final String userName = args[argctr++];
		final String apiKey = args[argctr++];
		final int k = Integer.parseInt(args[argctr++]);
		final List<String> tags = Arrays.asList(Arrays.copyOfRange(args, argctr++, args.length));

		final PostClusterer postClusterer = new PostClusterer(userName, apiKey);

		postClusterer.clusterPosts(userName, tags, k);
	}



	public List<Post<BibTex>> getAllPosts(final String userName, final List<String> tags) {
		int start = 0;
		final List<Post<BibTex>> result = new LinkedList<Post<BibTex>>();
		List<Post<BibTex>> posts;
		while ((posts = this.logic.getPosts(BibTex.class, GroupingEntity.USER, userName, tags, null, null, null, Sets.<Filter>asSet(FilterEntity.UNFILTERED), Order.ADDED, null, null, start, MAX_POSTS)).size() > 0) {
			result.addAll(posts);
			start += MAX_POSTS;
		}
		return result;
	}

	public void clusterPosts(final String userName, final List<String> tags, final int k) throws IOException {
		final List<Post<BibTex>> posts = getAllPosts(userName, tags);
		System.out.println("got " + posts.size() + " publications");
		for (final Post<BibTex> post : posts) {
			System.out.print("p");

		}
		System.out.println();
	}


	public static class KMeans<T> {

		public static interface Distance<T> {
			public double getDistance(final T a, final T b);
		}
		

//		public void kmeans (final int k, final List<T> items, final Distance<T> distance) {
//			final int n = items.size();
//			final int[] assignments = new int[n];
//			final int[] clusterSizes = new int[k];
//
//			final List<Map<String, Double>> centroids = getRandomCentroids(k, items);
//
//			boolean repeat = true;
//			int iter = 0;
//			while (repeat) {
//
//				// assignment step
//				for (int j = 0 ; j < k ; j++)
//					clusterSizes[j] = 0 ;
//
//				for (int i = 0 ; i < n ; i++) {
//					final T item = items.get(i) ;
//					// get closest item
//					double mindist = Double.MAX_VALUE;
//					int best;
//					for (int j = 0 ; j < k ; j++) {
//						final double dist = distance.getDistance(items.get(centroids.get(j)), item);
//						if (dist < mindist) {
//							mindist = dist ;
//							best = j ;
//						}
//					}
//					clusterSizes[best]++ ;
//					assignments[i] = best ;
//				}
//
//				// update centroids step
//				final List<Map<String, Double>> newCentroids = new ArrayList<Map<String, Double>>(k);
//
//				Vector<String> v = new Vec
//				
//				
//				
//				for (int i = 0 ; i < n ; i++) {
//					int cluster = assignments[i] ;
//					if (newCentroids.[cluster] == null)
//						newCentroids[cluster] = items.get(i) ;
//					else
//						newCentroids[cluster] = addVectors (newCentroids[cluster] , items.get(i)) ;       
//				}
//
//				for (var j = 0 ; j < k ; j++) {
//					newCentroids[j] = multiplyVectorByValue (1/clusterSizes[j] , newCentroids[j]) ;
//				}       
//
//				// check convergence
//				repeat = false ;
//				for (var j = 0 ; j < k ; j++) {
//					if (! newCentroids[j].compare (centroids[j])) {
//						repeat = true ; 
//						break ; 
//					}
//				}
//				centroids = newCentroids ;
//				nb_iters++ ;
//
//				// check nb of iters
//				if (nb_iters > figue.KMEANS_MAX_ITERATIONS)
//					repeat = false ;
//
//			}
//			return { 'centroids': centroids , 'assignments': assignments} ;
//
//		}



		private List<Integer> getRandomCentroids(final int k, final List<T> items) {
			if (k > items.size()) throw new IllegalArgumentException("more items than clusters (" + items.size() + " > " + k + ")"); 

			final Random rand = new Random();
			final List<Integer> centroids = new ArrayList<Integer>(k);

			while (centroids.size() < k) {
				final int randomIndex = rand.nextInt(items.size());
				if (centroids.contains(randomIndex))
					continue;
				centroids.add(randomIndex);
			}
			return centroids;
		}

	}
	


}
