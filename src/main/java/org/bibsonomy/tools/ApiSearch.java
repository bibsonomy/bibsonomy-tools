package org.bibsonomy.tools;

import java.util.List;

import org.bibsonomy.common.enums.GroupingEntity;
import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.Resource;
import org.bibsonomy.model.logic.LogicInterface;
import org.bibsonomy.rest.client.RestLogicFactory;

/**
 * @author rja
 */
public class ApiSearch {
	public static void main(String[] args) {
		final RestLogicFactory restLogicFactory = new RestLogicFactory();
		final LogicInterface logic = restLogicFactory.getLogicAccess("testuser", "testpassword");
		searchPosts(logic);
	}

	private static void searchPosts(final LogicInterface logic) {
		try {
//			final String query = "Searching Social Networks";
			final String query = "Personalized social query expansion using social bookmarking systems";
			
			final List<Post<BibTex>> posts = logic.getPosts(BibTex.class, GroupingEntity.ALL, null, null, query, null, null, null, null, null, null, 0, 20);

			System.out.println("got " + posts.size() + " posts");

			for (final Post<? extends Resource> post : posts) {
				final BibTex bibTex = (BibTex) post.getResource();
				System.out.println("'" + bibTex.getTitle() + "' by " + bibTex.getAuthor());
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
