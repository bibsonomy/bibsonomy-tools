/**
 *
 *  BibSonomy Tools - Useful tools for use with BibSonomy
 *
 *  Copyright (C) 2013 - 2013 Knowledge & Data Engineering Group,
 *                            University of Kassel, Germany
 *                            http://www.kde.cs.uni-kassel.de/
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.bibsonomy.tools;

import java.io.IOException;
import java.util.List;

import org.bibsonomy.common.enums.GroupingEntity;
import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.logic.LogicInterface;
import org.bibsonomy.rest.client.RestLogicFactory;

/**
 * Given a userName, apiKey and a query, download all posts that match the query.
 * 
 * @author:  rja
 * 
 */
public class PostSearcher {

	private static final int MAX_POSTS = 1000;

	private final LogicInterface logic;

	public PostSearcher(final String userName, final String apiKey) {
		this.logic = new RestLogicFactory().getLogicAccess(userName, apiKey);
	}
	
	public static void main(final String[] args) throws IOException {
		if (args.length < 2) {
			System.err.println("usage: java " + PostSearcher.class.getName() + " userName apiKey query");
			System.err.println("  retrieve the posts that match the given query");
			System.err.println("  (surround multi-term queries by quotes)");
			System.exit(1);
		}
		final String userName = args[0];
		final String apiKey = args[1];
		final String query = args[2];
		
		final PostSearcher postSearcher = new PostSearcher(userName, apiKey);
		postSearcher.queryPosts(query);
	}
	
	public void queryPosts(final String query) throws IOException {
		final List<Post<BibTex>> posts = this.logic.getPosts(BibTex.class, GroupingEntity.ALL, null, null, query, null, null, null, null, null, null, 0, MAX_POSTS);
		System.out.println("got " + posts.size() + " publication(s) for query '" + query + "'");
		
		for (final Post<BibTex> post : posts) {
			System.out.println("http://www.bibsonomy.org/bibtex/" + post.getResource().getIntraHash() + "/" + post.getUser());
		}
	}

}
