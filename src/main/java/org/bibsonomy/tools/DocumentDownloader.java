/**
 *
 *  BibSonomy Tools - Useful tools for use with BibSonomy
 *
 *  Copyright (C) 2013 - 2013 Knowledge & Data Engineering Group,
 *                            University of Kassel, Germany
 *                            http://www.kde.cs.uni-kassel.de/
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.bibsonomy.tools;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.bibsonomy.common.enums.Filter;
import org.bibsonomy.common.enums.FilterEntity;
import org.bibsonomy.common.enums.GroupingEntity;
import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.Document;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.enums.Order;
import org.bibsonomy.model.logic.LogicInterface;
import org.bibsonomy.rest.client.RestLogicFactory;
import org.bibsonomy.util.Sets;

/**
 * Given a userName, apiKey and (optionally) tags, downloads all documents of the
 * corresponding posts to the current directory.
 * 
 * @author:  rja
 * 
 */
public class DocumentDownloader {

	private static final int MAX_POSTS = 1000;

	private final LogicInterface logic;

	public DocumentDownloader(final String userName, final String apiKey) {
		this.logic = new RestLogicFactory().getLogicAccess(userName, apiKey);
	}
	
	public static void main(final String[] args) throws IOException {
		if (args.length < 2) {
			System.err.println("usage: java " + DocumentDownloader.class.getName() + " userName apiKey [tags]");
			System.err.println("download the documents of the posts with the given tag(s) to the current directory");
			System.exit(1);
		}
		final String userName = args[0];
		final String apiKey = args[1];
		final List<String> tags = Arrays.asList(Arrays.copyOfRange(args, 2, args.length));
		final String directory = "/home/rja/docs";
		
		final DocumentDownloader documentDownloader = new DocumentDownloader(userName, apiKey);
		
		documentDownloader.downloadDocuments(directory, userName, tags);
	}
	

	public void downloadDocuments(final String directory, final String userName, final List<String> tags) throws IOException {
		final List<Post<BibTex>> posts = getAllPosts(userName, tags);
		System.out.println("got " + posts.size() + " publications, downloading documents ");
		int documentCtr = 0;
		for (final Post<BibTex> post : posts) {
			System.out.print("p");
			final List<Document> documents = post.getResource().getDocuments();
			for (final Document document : documents) {
				System.out.print("d");
				documentCtr++;
				final String fileName = document.getFileName();
				final String intraHash = post.getResource().getIntraHash();
				final Document d = this.logic.getDocument(userName, intraHash, fileName);
				/*
				 * move file to directory
				 * TODO: create nice file name (e.g., including the publication year)
				 */
				final File dest = new File(directory + "/" + intraHash + "_" + fileName);
				final File source = d.getFile();
				FileUtils.moveFile(source, dest);
			}
		}
		System.out.println();
		System.out.println("wrote " + documentCtr + " documents to directory " + directory);
	}


	public List<Post<BibTex>> getAllPosts(final String userName, final List<String> tags) {
		int start = 0;
		final List<Post<BibTex>> result = new LinkedList<Post<BibTex>>();
		List<Post<BibTex>> posts;
		while ((posts = this.logic.getPosts(BibTex.class, GroupingEntity.USER, userName, tags, null, null, null, Sets.<Filter>asSet(FilterEntity.UNFILTERED), Order.ADDED, null, null, start, MAX_POSTS)).size() > 0) {
			result.addAll(posts);
			start += MAX_POSTS;
		}
		return result;
	}



}
