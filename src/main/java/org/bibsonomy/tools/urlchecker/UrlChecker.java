package org.bibsonomy.tools.urlchecker;

import java.net.UnknownHostException;
import java.util.Collections;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.impl.client.DefaultHttpClient;
import org.bibsonomy.common.enums.GroupingEntity;
import org.bibsonomy.common.enums.PostUpdateOperation;
import org.bibsonomy.model.Bookmark;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.Resource;
import org.bibsonomy.model.Tag;
import org.bibsonomy.model.logic.LogicInterface;

/**
 * checks all bookmarks of a user (for 404, …)
 * @author dzo
 */
public class UrlChecker {

	private static final int ITEMS = 100;
	private static final HttpClient CLIENT = new DefaultHttpClient();
	private static final String BROKEN = "sys:broken:";

	/**
	 * @param logic
	 */
	public void checkBookmarks(final LogicInterface logic) {
		List<Post<Bookmark>> posts;
		int start = 0;
		do {
			posts = logic.getPosts(Bookmark.class, GroupingEntity.USER, logic.getAuthenticatedUser().getName(), null, null, null, null, null, null, null, null, start, start + ITEMS);

			for (final Post<Bookmark> post : posts) {
				final Bookmark bookmark = post.getResource();

				final String url = bookmark.getUrl();

				final HttpHead head = new HttpHead(url);
				String error = null;
				try {
					final HttpResponse response = CLIENT.execute(head);
					final int code = response.getStatusLine().getStatusCode();
					switch (code) {
					case 200:
						// ok
						// System.out.println(url);
						break;
					case 404:
						System.err.println(url);
						error = "404";
						break;
					default:
						break;
					}
				} catch (final UnknownHostException e) {
					error = "404";
				} catch (final Exception e) {
					System.err.println(url);
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				if (error != null) {
					post.getTags().add(new Tag(BROKEN + error));
					logic.updatePosts(Collections.<Post<? extends Resource>>singletonList(post), PostUpdateOperation.UPDATE_ALL);
				}
			}

			start += ITEMS;
		} while (posts.size() == ITEMS);
	}
}
