package org.bibsonomy.tools.urlchecker;

import org.bibsonomy.model.logic.LogicInterface;
import org.bibsonomy.rest.client.RestLogicFactory;

/**
 * @author dzo
 */
public class UrlCheckerCommandLine {
	private static final RestLogicFactory FACTORY = new RestLogicFactory("https://www.bibsonomy.org/api/");

	public static void main(final String[] args) {
		if (args.length < 2 || args.length > 2) {
			// TODO: print usage
			System.exit(1);
		}

		final String username = args[0];
		final String apiKey = args[1];

		final LogicInterface logic = FACTORY.getLogicAccess(username, apiKey);

		final UrlChecker checker = new UrlChecker();
		checker.checkBookmarks(logic);
	}
}
