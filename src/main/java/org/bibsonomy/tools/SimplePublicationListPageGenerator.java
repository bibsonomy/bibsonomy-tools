package org.bibsonomy.tools;

import static org.bibsonomy.util.ValidationUtils.present;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.bibsonomy.common.enums.GroupingEntity;
import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.Document;
import org.bibsonomy.model.PersonName;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.logic.LogicInterface;
import org.bibsonomy.model.util.BibTexUtils;
import org.bibsonomy.model.util.PersonNameUtils;
import org.bibsonomy.rest.client.RestLogicFactory;
import org.bibsonomy.rest.client.util.FileFactory;

/**
 * generates a simple html site with all publications
 * downloads also all publications and adds links
 *
 * @author dzo
 */
public class SimplePublicationListPageGenerator {

	private static final int MAX_QUERY_SIZE = 1000;

	private static class HashFileFactory implements FileFactory {

		private final File basePath;

		private HashFileFactory(File basePath) {
			this.basePath = basePath;
		}

		@Override
		public File getFileForResourceDocument(String username, String hash, String filename) {
			return new File(this.basePath, hash + ".pdf");
		}
	}

	public static void main(String[] args) {
		final String username = args[0];
		final String apiKey = args[1];

		final String userToQuery = args[2];

		final File baseFilePath = new File(args[3]);
		if (!baseFilePath.exists()) {
			baseFilePath.mkdirs();
		}

		final String personName = args[4];
		final String authorFilter = args.length > 5 ? args[5] : null;

		final File htmlFile = new File(baseFilePath, "index.html");
		try (final BufferedWriter writer = new BufferedWriter(new FileWriter(htmlFile, false))) {
			writer.write("<!DOCTYPE html><html><head><meta charset=\"UTF-8\"><title>" + personName + " - Publications</title></head><body><h1>Publications of " + personName + "</h1><ul id=\"publication-list\">");
			final RestLogicFactory factory = new RestLogicFactory(new HashFileFactory(baseFilePath));
			final LogicInterface logic = factory.getLogicAccess(username, apiKey);

			int size;
			int start = 0;
			do {
				final List<Post<BibTex>> posts = logic.getPosts(BibTex.class, GroupingEntity.USER, userToQuery, Collections.singletonList("myown"), null, null, null, null, null, null, null, start, MAX_QUERY_SIZE);
				for (final Post<BibTex> post : posts) {
					final BibTex publication = post.getResource();
					final String hash = publication.getIntraHash();

					final List<PersonName> author = publication.getAuthor();
					final boolean isAuthor = present(author);
					final String authorStr = serializePersonNames(isAuthor ? author : publication.getEditor());
					if (present(authorFilter)) {
						if (!authorStr.contains(authorFilter)) {
							continue;
						}
					}

					boolean hasPDF = false;
					final String title = publication.getTitle();
					try {
						// filter documents
						final List<Document> allDocuments = publication.getDocuments();
						if (present(allDocuments)) {
							final List<Document> documents = allDocuments.stream().filter(doc -> doc.getFileName().endsWith(".pdf")).collect(Collectors.toList());

							hasPDF = present(documents);

							if (hasPDF) {
								if (documents.size() > 1) {
									System.out.println("multiple files found for " + title);
								}

								final Document document = documents.get(0);
								logic.getDocument(userToQuery, hash, document.getFileName());
							}
						}

						// write publication
						writer.append("<li>");
						writer.append(BibTexUtils.cleanBibTex(authorStr));
						if (!isAuthor) {
							writer.append(" (Eds.)");
						}
						writer.append(": ");
						if (hasPDF) {
							writer.append("<a href=\"" + hash + ".pdf\">");
						}
						writer.append(BibTexUtils.cleanBibTex(title));
						if (hasPDF) {
							writer.append("</a>");
						}
						writer.append(". ");
						writer.append(publication.getYear());

						writer.append("</li>");
					} catch(Exception e){
						System.err.println("can't add document to " + title);
						e.printStackTrace();
					}
				}
				size = posts.size();
				start += MAX_QUERY_SIZE;
			} while (size == MAX_QUERY_SIZE);

			writer.append("</ul></body></html>");
		} catch (final IOException e) {
			e.printStackTrace();
		}
	}

	private static String serializePersonNames(final List<PersonName> personNames) {
		return PersonNameUtils.serializePersonNames(personNames, "; ");
	}
}
