package org.bibsonomy.tools;

import static org.bibsonomy.util.ValidationUtils.present;

import java.io.FileInputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.bibsonomy.common.enums.GroupingEntity;
import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.Document;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.User;
import org.bibsonomy.model.logic.LogicInterface;
import org.bibsonomy.model.util.BibTexUtils;
import org.bibsonomy.rest.client.RestLogicFactory;
import org.bibsonomy.util.Sets;

/**
 * imports publications from group members to the group account
 *
 * @author dzo
 */
public class GroupAutoCopy {

	private static final int MAX_QUERY_SIZE = 1000;

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		try (final FileInputStream stream = new FileInputStream(args[0])) {
			final Properties properties = new Properties();
			properties.load(stream);
			
			final String url = properties.getProperty("system.url", RestLogicFactory.BIBSONOMY_API_URL);
			final RestLogicFactory factory = new RestLogicFactory(url);
			final String groupName = properties.getProperty("target.group.name");
			final String apiKey = properties.getProperty("target.group.apikey");

			final GroupingEntity groupingEntity = GroupingEntity.GROUP;
			final String groupingName = groupName;
			
			final List<String> tags;
			final String tagString = properties.getProperty("tags", null);
			if (tagString != null) {
				tags = Arrays.asList(tagString.split(","));
			} else {
				tags = null;
			}
			
			final Set<String> ignoredHashes;
			final String ignoredHashesString = properties.getProperty("ignore.publications", null);
			if (ignoredHashesString != null) {
				ignoredHashes = Sets.asSet(ignoredHashesString.split(","));
			} else {
				ignoredHashes = Collections.emptySet();
			}
			
			final LogicInterface logic = factory.getLogicAccess(groupName, apiKey);
			int size = 0;
			
			final Map<String, Post<BibTex>> postMap = new HashMap<String, Post<BibTex>>();
			final Set<String> groupPosts = new HashSet<String>();
			do {
				final List<Post<BibTex>> posts = logic.getPosts(BibTex.class, groupingEntity, groupingName, tags, null, null, null, null, null, null, null, 0, MAX_QUERY_SIZE);
				for (final Post<BibTex> post : posts) {
					String interHash = post.getResource().getInterHash();
					if (groupName.equals(post.getUser().getName())) {
						groupPosts.add(interHash);
					} else {
						/*
						 * prefer posts with documents
						 */
						boolean putInMap = true;
						
						if (postMap.containsKey(interHash)) {
							final Post<BibTex> knownPost = postMap.get(interHash);
							if (present(knownPost.getResource().getDocuments())) {
								putInMap = false;
							}
						}
						
						if (putInMap) {
							postMap.put(interHash, post);
						}
					}
				}
				size = posts.size();
			} while (size == MAX_QUERY_SIZE);
			
			final Set<String> sourceUserPosts = new HashSet<>(postMap.keySet());
			// remove all already copied posts
			sourceUserPosts.removeAll(groupPosts);
			// remove publications that should be ignored
			sourceUserPosts.removeAll(ignoredHashes);

			// get some configs for importing
			final boolean onlyCopyPostsWithDocuments = Boolean.parseBoolean(properties.getProperty("postsWithDocsOnly", "false"));
			final int waitingDays = Integer.parseInt(properties.getProperty("waitForDocumentsDays", "-1"));

			if (present(sourceUserPosts)) {
				final StringBuilder warnings = new StringBuilder();
				final StringBuilder imports = new StringBuilder();
				final User groupUser = new User(groupName);

				for (final String missingInterHash : sourceUserPosts) {
					final Post<BibTex> missingPost = postMap.get(missingInterHash);
					final String userName = missingPost.getUser().getName();
					missingPost.addTag("from:" + userName);

					final boolean waitNoLongerForDocuments = waitNoLongerForDocuments(missingPost, waitingDays);

					final BibTex publication = missingPost.getResource();
					
					missingPost.setUser(groupUser);
					final String title = BibTexUtils.cleanBibTex(publication.getTitle());
					
					final List<Document> documents = publication.getDocuments();
					final boolean documentPresent = present(documents);

					final String message = "- " + title + " (" + missingInterHash + ")";
					if (!onlyCopyPostsWithDocuments || documentPresent || waitNoLongerForDocuments) {
						imports.append(message);
						if (waitNoLongerForDocuments) {
							imports.append("\nTime is up! Adding post without document. @" + userName + " please add it yourself to the group post!");
						}
						imports.append("\n");
						logic.createPosts(Collections.singletonList(missingPost));
					} else {
						warnings.append(message);
						if (waitingDays >= 0) {
							final long daysRemaining = calcDaysRemaining(missingPost, waitingDays);
							warnings.append("\n @" + userName + " " + daysRemaining + " day(s) left till the post will be added without document and you have to add the document to two posts instead of only one!");
						}
						warnings.append("\n");
					}
					
					if (documentPresent) {
						final String intraHash = publication.getIntraHash();
						for (final Document document : documents) {
							final String fileName = document.getFileName();
							System.out.println("\t " + fileName);
							final Document downloadedDoc = logic.getDocument(userName, intraHash, fileName);
							downloadedDoc.setUserName(groupName);
							logic.createDocument(downloadedDoc, intraHash);
						}
					}
				}

				if (imports.length() > 0) {
					System.out.println("Importing the following publications");
					System.out.println(imports.toString());
				}
				
				if (onlyCopyPostsWithDocuments) {
					System.out.println("Skipped the following publications (no docs):");
					System.out.println(warnings.toString());
				}
			}
		}
		
	}

	private static long calcDaysRemaining(Post<BibTex> missingPost, int waitingDays) {
		final Date postDate = missingPost.getDate();
		LocalDate from = convertToLocalDate(postDate);
		LocalDate to = LocalDate.now();

		final long days = ChronoUnit.DAYS.between(from, to);
		return waitingDays - days;
	}

	private static boolean waitNoLongerForDocuments(Post<BibTex> missingPost, int waitingDays) {
		if (present(missingPost.getResource().getDocuments())) {
			return false;
		}

		if (waitingDays == -1) {
			return false;
		}

		return calcDaysRemaining(missingPost, waitingDays) <= 0;
	}

	private static LocalDate convertToLocalDate(Date dateToConvert) {
		return dateToConvert.toInstant()
						.atZone(ZoneId.systemDefault())
						.toLocalDate();
	}
}
