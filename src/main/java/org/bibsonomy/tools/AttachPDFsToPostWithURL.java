package org.bibsonomy.tools;

import static org.bibsonomy.util.ValidationUtils.present;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Collections;
import java.util.List;

import org.bibsonomy.common.enums.GroupingEntity;
import org.bibsonomy.common.enums.PostUpdateOperation;
import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.Document;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.logic.LogicInterface;
import org.bibsonomy.rest.client.RestLogicFactory;

/**
 * tool that iterates over all posts of a user, checks if the url is a pdf, downloads the specified pdf
 * and attaches it to the post
 *
 * @author dzo
 */
public class AttachPDFsToPostWithURL {
	public static void main(String[] args) {
		final RestLogicFactory restLogicFactory = new RestLogicFactory(args[0]);
		final String username = args[1];
		final LogicInterface logic = restLogicFactory.getLogicAccess(username, args[2]);

		final List<Post<BibTex>> posts = logic.getPosts(BibTex.class, GroupingEntity.USER, username, null, null, null, null, null, null, null, null, 0, 1000);
		final String tempDir = System.getProperty("java.io.tmpdir");
		for (final Post<BibTex> post : posts) {
			final BibTex publication = post.getResource();
			final String url = publication.getUrl();
			final List<Document> documents = publication.getDocuments();
			if (!present(documents) && present(url) && url.endsWith(".pdf")) {
				System.out.println("downloading " + url);
				try {
					final URL urlPath = new URL(url);
					final String path = urlPath.getPath();
					final String fileName =  path.substring(path.lastIndexOf('/') + 1);

					// download the file
					final URL urlToFile = new URL(url);
					InputStream in = urlToFile.openStream();
					final String file = tempDir + "/" + fileName;
					Files.copy(in, Paths.get(file), StandardCopyOption.REPLACE_EXISTING);
					in.close();

					final Document document = new Document();
					document.setFile(new File(file));
					document.setFileName(fileName);

					logic.createDocument(document, publication.getIntraHash());
					publication.setUrl(null);
					logic.updatePosts(Collections.singletonList(post), PostUpdateOperation.UPDATE_ALL);

				} catch (final Exception e) {
					System.err.println("can't add document to " + publication.getTitle());
					e.printStackTrace();
				}
			}
		}
	}
}